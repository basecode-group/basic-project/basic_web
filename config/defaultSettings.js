export default {
  navTheme: 'light',
  primaryColor: '#1890FF',
  layout: 'sidemenu',
  contentWidth: 'Fluid',
  fixedHeader: true,
  autoHideHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  menu: {
    locale: true,
  },
  title: 'Bacisic Admin',
  pwa: false,
  iconfontUrl: '',
  history: 'hash',
  http: 'http://localhost:8000'
  // http: 'http://192.168.1.75:8010'
};
