export default {
  'menu.welcome': '欢迎',
  'menu.more-blocks': '更多区块',
  'menu.home': '首页',
  'menu.admin': '管理页',
  'menu.login': '登录',
  'menu.register': '注册',
  'menu.register.result': '注册结果',
  'menu.account': '个人页',
  'menu.account.center': '个人中心',
  'menu.account.settings': '个人设置',
  'menu.account.trigger': '触发报错',
  'menu.account.logout': '退出登录',
  'menu.account.updatePwd': '修改密码',
  'menu.sysConfig': '系统管理',
  'menu.sysConfig.user': '用户管理',
  'menu.sysConfig.role': '角色管理',
  'menu.sysConfig.menu': '菜单管理',
  'menu.sysConfig.dict': '字典管理',
  'menu.sysConfig.office': '部门管理',
  'menu.sysConfig.dictDetail': '字典详情',
  'menu.系统管理': '系统管理',
  'menu.系统管理.用户管理': '用户管理',
  'menu.系统管理.角色管理': '角色管理',
  'menu.系统管理.菜单管理': '菜单管理',
  'menu.系统管理.字典管理': '字典管理',
};
