import { authority, getFakeCaptcha } from '@/services/login';
import { setAuthority } from '@/utils/authority';
import AxiosUtil from '@/tool/AxiosUtil';

const Model = {
  namespace: 'login',
  state: {
    status: undefined,
  },
  effects: {
    * getCaptcha({ payload }, { call }) {
      yield call(getFakeCaptcha, payload);
    },

    // eslint-disable-next-line require-yield
    * logout() {
      AxiosUtil.get('/sys/oauth/logout');
      window.location.href = '/login';
    },

    * authority(_, { call, put }) {
      const response = yield call(authority);
      yield put({
        type: 'authorityStatus',
        payload: response,
      });
    },
  },
  reducers: {
    authorityStatus(state, { payload }) {
      setAuthority(payload);
    },
  },
};
export default Model;
