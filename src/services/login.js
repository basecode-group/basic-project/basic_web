import request from '@/utils/request';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

export async function fakeAccountLogin(params) {
  return request(`${http}/sys/oauth/login`, {
    method: 'POST',
    data: params,
  });
}

export async function getFakeCaptcha(mobile) {
  return request(`/api/login/captcha?mobile=${mobile}`);
}

export async function authority() {
  return request(`${http}/api/verify`);
}
