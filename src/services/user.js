import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const { http } = defaultSettings;


export async function query() {
  return request('/api/users');
}
// export async function queryCurrent() {
//   return request('/sys/user/get');
// }
export async function queryNotices() {
  return request('/api/notices');
}

export async function queryCurrent() {
  return request(`${http}/sys/user/get`, {
    headers: { Authorization: localStorage.getItem('Authorization') },
  });
}
