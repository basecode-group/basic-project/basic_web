import React, { Component } from 'react';
import { Redirect } from 'umi';
import $ from 'jquery';
import defaultSettings from '../../config/defaultSettings';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const { http } = defaultSettings;

/**
 * 跳转首页
 */
// eslint-disable-next-line react/prefer-stateless-function
export default class welcome extends Component {
  state = {
    href: '/404',
  };

  componentWillMount() {
    this.setState({ href: '/404' });
    // eslint-disable-next-line no-underscore-dangle
    const _this = this;
    $.ajax({
      url: `${http}/sys/menu/get/for/user`,
      data: {},
      dataType: 'json',
      cache: false,
      async: false,
      headers: { Authorization: localStorage.getItem('Authorization') },
      type: 'GET',
      success (res) {
        let menuData = res.result;
        if (menuData.length > 0) {
          let children = menuData[0].children;
          console.log(children)
          if (children.length > 0) {
            _this.setState({ href: children[0].path });
          }
        }
      },
      error (err) {
        console.log(`菜单获取异常：${JSON.stringify(err)}`)
      },
    });
  }

  render() {
    return (
      <div>
        <Redirect to={this.state.href}/>
      </div>
    );
  }
}
