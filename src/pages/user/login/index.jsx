import { Alert, Checkbox, Icon, Spin } from 'antd';
import { FormattedMessage, formatMessage } from 'umi-plugin-react/locale';
import React, { Component } from 'react';
import Link from 'umi/link';
import { connect } from 'dva';
import LoginComponents from './components/Login';
import styles from './style.less';
import $ from 'jquery';
import defaultSettings from '../../../../config/defaultSettings';
import AxiosUtil from '@/tool/AxiosUtil';
import { notification } from 'antd';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const { http } = defaultSettings;

const { Tab, UserName, Password, Mobile, Captcha, Submit } = LoginComponents;

@connect(({ login, loading }) => ({
  userLogin: login,
  submitting: loading.effects['login/login'],
}))
class Login extends Component {
  loginForm = undefined;

  state = {
    type: 'account',
    autoLogin: true,
    spinning: false,
  };

  changeAutoLogin = e => {
    this.setState({
      autoLogin: e.target.checked,
    });
  };

  handleSubmit = (err, values) => {
    if (!err) {
      this.setState({ spinning: true });
      // eslint-disable-next-line no-underscore-dangle
      const _this = this;
      $.ajax({
        url: `${http}/sys/oauth/login`,
        data: values,
        dataType: 'json',
        cache: false,
        async: false,
        type: 'GET',
        success(res) {
          if (res.code !== '000') {
            // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
            notification.error({
              message: '请求失败',
              description: res.msg !== undefined ? res.msg : '系统异常！',
            });
            _this.setState({ spinning: false });
            return;
          }
          localStorage.setItem('Authorization', res.result.tails.access_token);
          window.location.href = '/welcome';
        },
        // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
        error(err) {
          _this.setState({ spinning: false });
          // eslint-disable-next-line no-undef
          notification.error({
            message: '请求失败',
            description: AxiosUtil.getErrMsg(err),
          });
        },
      });
    }
  };

  onTabChange = type => {
    this.setState({
      type,
    });
  };

  onGetCaptcha = () =>
    new Promise((resolve, reject) => {
      if (!this.loginForm) {
        return;
      }

      this.loginForm.validateFields(['mobile'], {}, async (err, values) => {
        if (err) {
          reject(err);
        } else {
          const { dispatch } = this.props;

          try {
            const success = await dispatch({
              type: 'login/getCaptcha',
              payload: values.mobile,
            });
            resolve(!!success);
          } catch (error) {
            reject(error);
          }
        }
      });
    });

  renderMessage = content => (
    <Alert
      style={{
        marginBottom: 24,
      }}
      message={content}
      type="error"
      showIcon
    />
  );

  render() {
    const { userLogin, submitting } = this.props;
    const { status, type: loginType } = userLogin;
    const { type, autoLogin, spinning } = this.state;
    return (
      <div className={styles.main}>
        <Spin spinning={spinning}>
          <LoginComponents
            defaultActiveKey={type}
            onTabChange={this.onTabChange}
            onSubmit={this.handleSubmit}
            onCreate={form => {
              this.loginForm = form;
            }}
          >
            <Tab
              key="account"
              tab={formatMessage({
                id: 'user-login.login.tab-login-credentials',
              })}
            >
              {status === 'error' &&
              loginType === 'account' &&
              !submitting &&
              this.renderMessage(
                formatMessage({
                  id: 'user-login.login.message-invalid-credentials',
                }),
              )}
              <UserName
                name="username"
                placeholder={`${formatMessage({
                  id: 'user-login.login.userName',
                })}: admin or user`}
                rules={[
                  {
                    required: true,
                    message: formatMessage({
                      id: 'user-login.userName.required',
                    }),
                  },
                ]}
              />
              <Password
                name="password"
                placeholder={`${formatMessage({
                  id: 'user-login.login.password',
                })}: ant.design`}
                rules={[
                  {
                    required: true,
                    message: formatMessage({
                      id: 'user-login.password.required',
                    }),
                  },
                ]}
                onPressEnter={e => {
                  e.preventDefault();

                  if (this.loginForm) {
                    this.loginForm.validateFields(this.handleSubmit);
                  }
                }}
              />
            </Tab>
            <Tab
              key="mobile"
              tab={formatMessage({
                id: 'user-login.login.tab-login-mobile',
              })}
            >
              {status === 'error' &&
              loginType === 'mobile' &&
              !submitting &&
              this.renderMessage(
                formatMessage({
                  id: 'user-login.login.message-invalid-verification-code',
                }),
              )}
              <Mobile
                name="mobile"
                placeholder={formatMessage({
                  id: 'user-login.phone-number.placeholder',
                })}
                rules={[
                  {
                    required: true,
                    message: formatMessage({
                      id: 'user-login.phone-number.required',
                    }),
                  },
                  {
                    pattern: /^1\d{10}$/,
                    message: formatMessage({
                      id: 'user-login.phone-number.wrong-format',
                    }),
                  },
                ]}
              />
              <Captcha
                name="captcha"
                placeholder={formatMessage({
                  id: 'user-login.verification-code.placeholder',
                })}
                countDown={120}
                onGetCaptcha={this.onGetCaptcha}
                getCaptchaButtonText={formatMessage({
                  id: 'user-login.form.get-captcha',
                })}
                getCaptchaSecondText={formatMessage({
                  id: 'user-login.captcha.second',
                })}
                rules={[
                  {
                    required: true,
                    message: formatMessage({
                      id: 'user-login.verification-code.required',
                    }),
                  },
                ]}
              />
            </Tab>
            <div>
              <Checkbox checked={autoLogin} onChange={this.changeAutoLogin}>
                <FormattedMessage id="user-login.login.remember-me"/>
              </Checkbox>
              <a
                style={{
                  float: 'right',
                }}
                href=""
              >
                <FormattedMessage id="user-login.login.forgot-password"/>
              </a>
            </div>
            <Submit loading={submitting}>
              <FormattedMessage id="user-login.login.login"/>
            </Submit>
            <div className={styles.other}>
              <FormattedMessage id="user-login.login.sign-in-with"/>
              <Icon type="alipay-circle" className={styles.icon} theme="outlined"/>
              <Icon type="taobao-circle" className={styles.icon} theme="outlined"/>
              <Icon type="weibo-circle" className={styles.icon} theme="outlined"/>
              <Link className={styles.register} to="/user/register">
                <FormattedMessage id="user-login.login.signup"/>
              </Link>
            </div>
          </LoginComponents>
        </Spin>
      </div>
    );
  }
}

export default Login;
