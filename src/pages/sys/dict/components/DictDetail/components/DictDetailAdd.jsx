import React, { Component } from 'react';
import { Form, Button, Modal, Input, InputNumber, message } from 'antd';
import AxiosUtil from '@/tool/AxiosUtil';

/**
 * 添加字典
 *
 * @author zhangby
 * @date 24/11/19 9:36 pm
 */
// eslint-disable-next-line react/prefer-stateless-function
class DictDetailAdd extends Component {
  state = { visible: false, loading: false };

  // 初始化
  addInit() {
    this.props.form.resetFields();
    const { setFieldsValue } = this.props.form;
    AxiosUtil.get('/sys/dict/max/sort', { parentId: this.props.dictId }).then(res => {
      setFieldsValue({ sort: res.result.maxSort });
    });
    this.handleOpen();
  }

  // 提交
  // eslint-disable-next-line react/sort-comp,@typescript-eslint/no-unused-vars
  handleSubmit = e => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      // eslint-disable-next-line no-empty
      values['parentId'] = this.props.dictId;
      values['type'] = this.props.dictType;
      if (!err) {
        this.setState({ loading: true });
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        AxiosUtil.post('/sys/dict', values).then(res => {
          message.success('新建字典成功');
          this.handleCancel();
          this.props.queryDictDetailsList();
          this.setState({ loading: false });
        }).catch(() => {
          this.setState({ loading: false });
        })
      }
    });
  };

  // eslint-disable-next-line react/sort-comp
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };

    return (
      <span>
        {/* eslint-disable-next-line react/jsx-no-bind */}
        <Button icon="plus" type="primary" onClick={this.addInit.bind(this)}>新建</Button>
        {/* 弹出层 */}
        <Modal
          title="新建字典"
          visible={this.state.visible}
          onOk={this.handleSubmit}
          onCancel={this.handleCancel}
          confirmLoading = {this.state.loading}
          width={600}
        >
         <Form {...formItemLayout}>
          <Form.Item label="字典名称">
            {getFieldDecorator('label', {
              rules: [
                {
                  required: true,
                  message: '请输入字典名称',
                },
              ],
            })(<Input/>)}
          </Form.Item>
          <Form.Item label="字典值">
            {getFieldDecorator('value', {
              rules: [
                {
                  required: true,
                  message: '请输入字典值',
                },
              ],
            })(<Input/>)}
          </Form.Item>
          <Form.Item label="排序">
            {getFieldDecorator('sort', {
              initialValue: 10,
              rules: [
                {
                  required: true,
                  message: '请输入排序值',
                },
              ],
            })(<InputNumber min={0}/>)}
          </Form.Item>
          <Form.Item label="描述">
            {getFieldDecorator('description')(<Input.TextArea rows={3} />)}
          </Form.Item>
         </Form>
        </Modal>
      </span>
    )
  }


  // eslint-disable-next-line react/sort-comp,@typescript-eslint/no-unused-vars
  handleOpen = e => {
    this.setState({ visible: true });
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  handleCancel = e => {
    this.setState({ visible: false });
  };
}

export default Form.create({ name: 'DictDetailAdd' })(DictDetailAdd);
