import React, { Component } from 'react';
import { Form, Button, Modal, Input, InputNumber, message } from 'antd';
import AxiosUtil from '@/tool/AxiosUtil';

/**
 * 添加字典
 *
 * @author zhangby
 * @date 24/11/19 9:36 pm
 */
// eslint-disable-next-line react/prefer-stateless-function
class DictDetailEdit extends Component {
  state = { visible: false, loading: false };

  // 初始化
  addInit() {
    this.props.form.resetFields();
    const { setFieldsValue } = this.props.form;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    AxiosUtil.get(`/sys/dict/${this.props.dictId}`).then(res => {
      const val = res.result;
      setFieldsValue({
        label: val.label,
        value: val.value,
        description: val.description,
        sort: val.sort,
      });
      this.setState({ loading: false });
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
    }).catch(err => {
      this.setState({ loading: false });
    });
    this.handleOpen();
  }

  // 提交
  // eslint-disable-next-line react/sort-comp,@typescript-eslint/no-unused-vars
  handleSubmit = e => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      // eslint-disable-next-line no-empty
      if (!err) {
        this.setState({ loading: true });
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        values['type'] = this.props.dictType;
        AxiosUtil.put(`/sys/dict/${this.props.dictId}`, values).then(res => {
          message.success('更新字典成功');
          this.handleCancel();
          this.props.queryDictDetailsList();
          this.setState({ loading: false });
        }).catch(() => {
          this.setState({ loading: false });
        })
      }
    });
  };

  // eslint-disable-next-line react/sort-comp
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };

    return (
      <span>
        {/* eslint-disable-next-line react/jsx-no-bind */}
        <Button type="primary" shape="circle" icon="edit" style={{ marginRight: 10 }} onClick={this.addInit.bind(this)}/>
        {/* 弹出层 */}
        <Modal
          title="编辑字典"
          visible={this.state.visible}
          onOk={this.handleSubmit}
          onCancel={this.handleCancel}
          confirmLoading = {this.state.loading}
          width={600}
        >
         <Form {...formItemLayout}>
          <Form.Item label="字典名称">
            {getFieldDecorator('label', {
              rules: [
                {
                  required: true,
                  message: '请输入字典名称',
                },
              ],
            })(<Input/>)}
          </Form.Item>
          <Form.Item label="字典值">
            {getFieldDecorator('value', {
              rules: [
                {
                  required: true,
                  message: '请输入字典值',
                },
              ],
            })(<Input/>)}
          </Form.Item>
          <Form.Item label="排序">
            {getFieldDecorator('sort', {
              initialValue: 10,
              rules: [
                {
                  required: true,
                  message: '请输入排序值',
                },
              ],
            })(<InputNumber min={0}/>)}
          </Form.Item>
          <Form.Item label="描述">
            {getFieldDecorator('description')(<Input.TextArea rows={3} />)}
          </Form.Item>
         </Form>
        </Modal>
      </span>
    )
  }


  // eslint-disable-next-line react/sort-comp,@typescript-eslint/no-unused-vars
  handleOpen = e => {
    this.setState({ visible: true });
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  handleCancel = e => {
    this.setState({ visible: false });
  };
}

export default Form.create({ name: 'DictDetailEdit' })(DictDetailEdit);
