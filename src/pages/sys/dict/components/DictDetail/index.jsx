import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Table, Row, Col, Input, Button, Popconfirm, Icon, message } from 'antd';
import router from 'umi/router';
import AxiosUtil from '@/tool/AxiosUtil';
import DictDetailAdd from '@/pages/sys/dict/components/DictDetail/components/DictDetailAdd';
import DictDetailEdit from '@/pages/sys/dict/components/DictDetail/components/DictDetailEdit';

const { Column } = Table;

// eslint-disable-next-line react/prefer-stateless-function
export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      params: this.props.match.params,
      pagination: {
        pageSize: 10,
        current: 1,
        total: 0,
        defaultPageSize: 10,
        showSizeChanger: true,
        onChange: (page, pageSize) => {
          this.state.query.pageNum = page;
          this.state.query.pageSize = pageSize;
          // eslint-disable-next-line react/no-access-state-in-setstate
          this.setState(this.state);
          this.queryDictList();
        },
        onShowSizeChange: (page, pageSize) => {
          this.state.query.pageNum = page;
          this.state.query.pageSize = pageSize;
          // eslint-disable-next-line react/no-access-state-in-setstate
          this.setState(this.state);
          this.queryDictList();
        },
      },
      loading: false,
      query: {
        keyword: '',
        parentId: this.props.match.params.dictId,
      },
    };
    this.queryDictDetailsList = this.queryDictDetailsList.bind(this);
  }

  componentWillMount() {
    this.queryDictDetailsList();
  }

  queryDictDetailsList() {
    const { query } = this.state;
    this.setState({ loading: true });
    AxiosUtil.get('/sys/dict', query)
      .then(res => {
        const val = res.result;
        this.setState({
          pagination: {
            pageSize: val.size,
            current: val.current,
            total: val.total,
          },
          list: val.records,
          loading: false,
        })
      })
      .catch(() => {
        this.setState({ loading: false });
      })
  }

  // eslint-disable-next-line class-methods-use-this
  deleteConfirm(id) {
    this.setState({ loading: true });
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    AxiosUtil.delete(`/sys/dict/${id}`).then(res => {
      message.success('删除字典成功');
      this.queryDictDetailsList();
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
    }).catch(err => {
      this.setState({ loading: false });
    })
  }

  render() {
    const { params } = this.state;
    return (
      <div>
        <PageHeaderWrapper title="字典详情" onBack={() => router.push('/sys/dict')}
                           subTitle={<span style={{ marginLeft: 10 }}>{this.state.params.dictLabel}（ {this.state.params.dictType} ）</span>}>
          <Card>
            {/* 查询条件 */}
            <Row style={{ marginBottom: 20 }}>
              <Col span={12}>
                {/* eslint-disable-next-line max-len */}
                <DictDetailAdd queryDictDetailsList={this.queryDictDetailsList} dictId={params.dictId} dictType={params.dictType} />
              </Col>
              <Col span={12} style={{ textAlign: 'right' }}>
                <Input.Search placeholder="字典名称 / 字典值" style={{ maxWidth: 450 }} onSearch={val => {
                  this.state.query.keyword = val;
                  this.setState(this.state);
                  this.queryDictDetailsList();
                }} enterButton />
              </Col>
            </Row>
            {/* 数据列表 */}
            <Table dataSource={this.state.list} scroll={{ x: 900 }} loading={this.state.loading}
                   pagination={this.state.pagination}
                   rowKey="id">
              <Column align="center" title="字典名称" dataIndex="label" render={this.detailRender}/>
              <Column align="center" title="字典值" dataIndex="value" render={this.detailRender}/>
              <Column align="center" title="描述" dataIndex="description"/>
              <Column align="center" title="排序" dataIndex="sort"/>
              {/* eslint-disable-next-line @typescript-eslint/no-unused-vars */}
              <Column align="center" title="操作" dataIndex="id" render={id => (
                <div>
                  {/* eslint-disable-next-line max-len */}
                  <DictDetailEdit queryDictDetailsList={this.queryDictDetailsList} dictId={id} dictType={params.dictType} />
                  {/* 删除 */}
                  <Popconfirm
                    onConfirm = {this.deleteConfirm.bind(this, id)}
                    title="确定要删除记录吗？"
                    icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
                  >
                    <Button type="danger" shape="circle" icon="delete"/>
                  </Popconfirm>
                </div>
              )}/>
            </Table>
          </Card>
        </PageHeaderWrapper>
      </div>
    );
  }
}
