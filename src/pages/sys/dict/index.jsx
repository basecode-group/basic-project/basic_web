import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {
  Button,
  Card,
  Form,
  Input,
  Table,
  Alert,
  Tag,
  Popconfirm,
  Icon,
  message,
  Modal,
  Row,
  Col,
} from 'antd';
import router from 'umi/router';
import AxiosUtil from '@/tool/AxiosUtil';
import DictAdd from '@/pages/sys/dict/components/DictAdd';
import DictEdit from '@/pages/sys/dict/components/DictEdit';

const { Column } = Table;

/**
 * 字典管理
 * @author zhangby
 * @date 23/11/19 11:07 pm
 */
// eslint-disable-next-line react/prefer-stateless-function
class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      rows: [],
      pagination: {
        pageSize: 10,
        current: 1,
        total: 0,
        defaultPageSize: 10,
        showSizeChanger: true,
        onChange: (page, pageSize) => {
          this.state.query.pageNum = page;
          this.state.query.pageSize = pageSize;
          // eslint-disable-next-line react/no-access-state-in-setstate
          this.setState(this.state);
          this.queryDictList();
        },
        onShowSizeChange: (page, pageSize) => {
          this.state.query.pageNum = page;
          this.state.query.pageSize = pageSize;
          // eslint-disable-next-line react/no-access-state-in-setstate
          this.setState(this.state);
          this.queryDictList();
        },
      },
      loading: false,
      query: {
        label: '',
        type: '',
      },
    };
    this.queryDictList = this.queryDictList.bind(this);
  }

  // 初始化
  componentDidMount() {
    this.queryDictList();
  }

  // 查询字典列表
  queryDictList() {
    this.setState({ loading: true });
    AxiosUtil.get('/sys/dict', this.state.query)
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      .then(res => {
        const val = res.result;
        this.setState({
          pagination: {
            pageSize: val.size,
            current: val.current,
            total: val.total,
          },
          list: val.records,
          loading: false,
        })
      })
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      .catch(() => {
        this.setState({ loading: false });
      })
  }

  // 批量删除
  // eslint-disable-next-line class-methods-use-this
  deleteConfirm(id) {
    AxiosUtil.delete(`/sys/dict/${id}`)
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      .then(res => {
        message.success('删除字典成功');
        this.queryDictList();
      });
  }

  // eslint-disable-next-line class-methods-use-this
  deleteBachConfirm() {
    const { rows } = this.state;
    // eslint-disable-next-line no-underscore-dangle
    const _this = this;
    Modal.confirm({
      title: '确定要删除选择的记录吗?',
      content: `已选择${rows.length}项记录。`,
      onOk() {
        AxiosUtil.delete(`/sys/dict/${rows.join(',')}`)
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          .then(res => {
            message.success('删除字典成功');
            _this.queryDictList();
          })
      },
    });
  }

  // eslint-disable-next-line react/sort-comp
  render() {
    const formItemLayout = {
      labelCol: {
        span: 6,
        offset: 0,
      },
      wrapperCol: {
        span: 6,
        offset: 0,
      },
    };
    return (
      <div>
        <PageHeaderWrapper>
          <Card>
            <Form layout="inline" {...formItemLayout}>
              <Row gutter={10}>
                <Col xs={24} sm={16} md={12} lg={8}>
                  <Form.Item label="字典名称">
                    <Input placeholder="名称" width={250} style={{ width: 220 }} onChange={val => {
                      this.state.query.label = val.target.value;
                      // eslint-disable-next-line react/no-access-state-in-setstate
                      this.setState(this.state);
                    }}/>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={16} md={12} lg={8}>
                  <Form.Item label="字典类型">
                    <Input placeholder="类型" style={{ width: 250 }} onChange={val => {
                      this.state.query.type = val.target.value;
                      // eslint-disable-next-line react/no-access-state-in-setstate
                      this.setState(this.state);
                    }}/>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={16} md={12} lg={8}>
                  <Form.Item>
                    <Button icon="search" type="primary" onClick={this.queryDictList}>查询</Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
            {/* 功能菜单 */}
            <div style={{ marginTop: 20 }}>
              {/* 新建 */}
              <DictAdd queryDictList={this.queryDictList}/>
              {/* 隐藏功能 */}
              {
                this.state.rows.length > 0 ?
                  <Button type="danger" icon="delete" style={{ marginLeft: 10 }}
                          onClick={this.deleteBachConfirm.bind(this)}>批量删除</Button>
                  : null
              }
            </div>
            <Alert style={{ margin: '20px 0 20px 0' }}
                   message={
                     <div>
                       已选择 <span style={{
                       color: '#1690ff',
                       fontWeight: 600,
                       margin: '0 5px',
                     }}>{this.state.rows.length}</span> 项
                       <span style={{
                         color: '#1690ff',
                         marginLeft: 30,
                         cursor: 'pointer',
                       }} onClick={() => this.setState({ rows: [] })}>清空</span>
                     </div>
                   } type="info" showIcon/>
            {/* 列表查询 */}
            <div style={{ marginTop: 30 }}>
              <Table dataSource={this.state.list} scroll={{ x: 900 }} loading={this.state.loading}
                     pagination={this.state.pagination}
                     rowKey="id"
                     rowSelection={{
                       selectedRowKeys: this.state.rows,
                       onChange: selectedRowKeys => {
                         this.setState({ rows: selectedRowKeys })
                       },
                     }}
              >
                <Column align="center" title="字典名称" dataIndex="label" render={this.detailRender}/>
                <Column align="center" title="字典类型" dataIndex="type" render={this.detailRender}/>
                <Column title="描述" dataIndex="description"/>
                <Column align="center" title="排序" dataIndex="sort"/>
                <Column align="center" title="创建时间" dataIndex="createDate"
                        render={this.textRender}/>
                {/* eslint-disable-next-line @typescript-eslint/no-unused-vars */}
                <Column align="center" title="操作" dataIndex="id" render={id => (
                  <div>
                    <DictEdit dictId={id} queryDictList={this.queryDictList}/>
                    {/* 删除 */}
                    <Popconfirm
                      onConfirm={this.deleteConfirm.bind(this, id)}
                      title="确定要删除记录吗？"
                      icon={<Icon type="question-circle-o" style={{ color: 'red' }}/>}
                    >
                      <Button type="danger" shape="circle" icon="delete"/>
                    </Popconfirm>
                  </div>
                )}/>
              </Table>
            </div>
          </Card>
        </PageHeaderWrapper>
      </div>
    )
  }

  // eslint-disable-next-line class-methods-use-this
  textRender(val) {
    return (<Tag.CheckableTag style={{ fontsize: 12 }}>{val}</Tag.CheckableTag>)
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars,class-methods-use-this
  detailRender(val, record) {
    return (<Button type="link"
                    onClick={() => router.push(`/sys/dict/detail/${record.type}/${record.label}/${record.id}`)}>{val}</Button>)
  }
}

export default Form.create({ name: 'index' })(index);
