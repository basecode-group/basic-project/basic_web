import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Form } from 'antd';

/**
 * 部门管理
 */
// eslint-disable-next-line react/prefer-stateless-function
class index extends Component {
  render() {
    return (
      <div>
        <PageHeaderWrapper>
          部门管理
        </PageHeaderWrapper>
      </div>
    )
  }
}

export default Form.create({ name: 'index' })(index);
