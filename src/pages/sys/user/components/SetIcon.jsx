import React, { Component } from 'react';
import { Avatar, Icon, Modal, Radio, Row, Col, Input } from 'antd';
import { TwitterPicker } from 'react-color';

// eslint-disable-next-line import/no-unresolved
import icon1 from '../../../../assets/avatars/1.jpg';
// eslint-disable-next-line import/no-unresolved
import icon2 from '../../../../assets/avatars/2.jpg';
// eslint-disable-next-line import/no-unresolved
import icon3 from '../../../../assets/avatars/3.jpg';
// eslint-disable-next-line import/no-unresolved
import icon4 from '../../../../assets/avatars/4.jpg';
// eslint-disable-next-line import/no-unresolved
import icon5 from '../../../../assets/avatars/5.jpg';
// eslint-disable-next-line import/no-unresolved
import icon6 from '../../../../assets/avatars/6.jpg';
// eslint-disable-next-line import/no-unresolved
import icon7 from '../../../../assets/avatars/7.jpg';
// eslint-disable-next-line import/no-unresolved
import icon8 from '../../../../assets/avatars/8.jpg';
// eslint-disable-next-line import/no-unresolved
import icon9 from '../../../../assets/avatars/9.png';
// eslint-disable-next-line import/no-unresolved
import icon10 from '../../../../assets/avatars/10.jpeg';
// eslint-disable-next-line import/no-unresolved
import icon11 from '../../../../assets/avatars/11.jpeg';


/**
 *  选择头像
 *  回调方法 function renderFun(String)
 *  初始化  photo
 *  是否启用编辑 isEdit false
 *  尺寸，  size small larger
 */
export default class SetIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      defaultColors: ['#FF6900', '#FCB900', '#7BDCB5', '#00D084', '#8ED1FC', '#0693E3', '#00a2ae', '#EB144C', '#F78DA7', '#9900EF'],
      background: '#cccccc',
      fontColor: '#ffffff',
      fontValue: '',
      select: true,
      key: 0,
      showData: {
        background: '#cccccc',
        fontColor: '#ffffff',
        fontValue: '',
        select: true,
        key: 0,
      },
    }
  }

  // 初始化
  componentWillMount() {
    if (this.props.photo !== undefined && this.props.photo !== '') {
      try {
        const photo = JSON.parse(this.props.photo);
        if (photo.select) {
          this.setState({
            select: photo.select,
            key: photo.key,
            showData: {
              select: photo.select,
              key: photo.key,
            },
          })
        } else {
          this.setState({
            select: photo.select,
            background: photo.background,
            fontColor: photo.fontColor,
            fontValue: photo.fontValue,
            showData: {
              select: photo.select,
              background: photo.background,
              fontColor: photo.fontColor,
              fontValue: photo.fontValue,
            },
          })
        }
        // eslint-disable-next-line no-empty
      } catch (err) {}
    }
  }

  // 初始化数据
  setIconInit() {
    if (this.props.isEdit) {
      this.handleOpen();
    }
  }

  // 结果
  handleSubmit() {
    const { background, fontColor, fontValue, select, key } = this.state;
    this.setState({
      showData: {
        background: background,
        fontColor: fontColor,
        fontValue: fontValue,
        select: select,
        key: key,
      },
    });
    const iconInfo = { select: select };
    if (select) {
      iconInfo['key'] = key;
    } else {
      iconInfo['background'] = background;
      iconInfo['fontColor'] = fontColor;
      iconInfo['fontValue'] = fontValue;
    }
    if (this.props.renderFun !== undefined) {
      this.props.renderFun(JSON.stringify(iconInfo));
    }
    this.handleCancel();
  }

  handleChange(color, event) {
    this.setState({ background: color.hex })
  }

  selectIcon(key) {
    this.setState({ key: key })
  }


  render() {
    return (
      // eslint-disable-next-line react/style-prop-object
      <span style = {{ ...this.props.style }}>
        <span style={{ cursor: 'pointer' }} onClick={this.setIconInit.bind(this)}>
          {
            this.state.showData.select ?
              <span> {
                this.props.size !== undefined && this.props.size === 'small' ?
                  // eslint-disable-next-line max-len
                  // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
                  defaultIcon[this.state.showData.key] : this.props.size !== undefined && this.props.size === 'mini' ?
                  // eslint-disable-next-line max-len
                  // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
                    defaultIcon25[this.state.showData.key] : defaultIcon50[this.state.showData.key]
              } </span>
              :
              <Avatar size={this.props.size !== undefined && this.props.size === 'small' ? 32 :
                this.props.size !== undefined && this.props.size === 'mini' ? 25 : 50}
                      style={{
                        backgroundColor: this.state.showData.background,
                        color: this.state.showData.fontColor,
                      }}>
                {
                  this.state.showData.fontValue !== '' ?
                    <div>{this.state.showData.fontValue}</div>
                    :
                    <Icon type="user"
                          style={{ fontSize: this.props.size !== undefined && this.props.size === 'small' ? 18 : 25 }}/>
                }
              </Avatar>
          }
        </span>

        <Modal
          title="Setting Icon"
          visible={this.state.visible}
          onOk={this.handleSubmit.bind(this)}
          onCancel={this.handleCancel}
          width={700}
        >
          <Row gutter={10}>
            <Col span={24} style={{ textAlign: 'center' }}>
              {
                this.state.select ?
                  <div> {defaultIcon50[this.state.key]} </div>
                  :
                  <Avatar size={50}
                          style={{
                            cursor: 'pointer',
                            backgroundColor: this.state.background,
                            color: this.state.fontColor,
                          }}>
                    {
                      this.state.fontValue !== '' ?
                        <div>{this.state.fontValue}</div>
                        :
                        <Icon type="user" style={{ fontSize: 25 }}/>
                    }
                  </Avatar>
              }
            </Col>
          </Row>

          <div style={{
            marginTop: 20,
            textAlign: 'center',
          }}>
            <Radio.Group
              defaultValue={this.state.select}
              size="small"
              onChange={(e) => this.setState({ select: e.target.value })}>
              <Radio.Button value={true}>system</Radio.Button>
              <Radio.Button value={false}>customize</Radio.Button>
            </Radio.Group>
          </div>

          {
            this.state.select ?
              <div>
                <Row gutter={10} style={{
                  marginTop: 20,
                  textAlign: 'center',
                }}>
                  {
                    // eslint-disable-next-line max-len
                    // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
                    defaultIcon.map((item, key) => (
                      <Col md={3} xs={6} key={key}
                           style={{
                             margin: '8px 0 8px 0',
                             cursor: 'pointer',
                           }}
                           onClick={this.selectIcon.bind(this, key)}>
                        {item}
                      </Col>
                    ))
                  }
                </Row>
              </div>
              :
              <Row gutter={10} style={{ marginTop: 20 }}>
                <Col md={12} xs={24} style={{ marginBottom: 10 }}>
                  <div style={{ marginLeft: 60 }}>
                    <TwitterPicker
                      width={205}
                      triangle="hide"
                      colors={[...this.state.defaultColors, '#cccccc']}
                      onChange={this.handleChange.bind(this)}
                      color={this.state.background}/>
                    <div style={{
                      marginLeft: 45,
                      fontWeight: 700,
                      color: '#999'
                    }}> BackgroundColor
                    </div>
                  </div>
                </Col>
                <Col md={12} xs={24} style={{ marginBottom: 10 }}>
                  <div style={{ marginLeft: 60 }}>
                    <TwitterPicker
                      width={205}
                      triangle="hide"
                      onChange={(color) => this.setState({ fontColor: color.hex })}
                      colors={[...this.state.defaultColors, '#ffffff']}
                      color={this.state.fontColor}/>
                    <div style={{
                      marginLeft: 65,
                      fontWeight: 700,
                      color: '#999'
                    }}> FontColor
                    </div>
                  </div>
                </Col>
                <Col span={24} style={{ marginBottom: 20 }}>
                  <Row>
                    <Col span={12} style={{
                      textAlign: 'right',
                      fontWeight: 700,
                      color: '#999',
                      marginTop: 5
                    }}>
                      ICON:&nbsp;&nbsp;&nbsp;&nbsp;
                    </Col>
                    <Col span={12}>
                      <Input
                        style={{ width: 100 }}
                        placeholder="ICON"
                        value={this.state.fontValue}
                        onChange={(e) => this.setState({ fontValue: e.target.value })}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
          }

        </Modal>
      </span>
    )
  }

  // 打开关闭
  // eslint-disable-next-line react/sort-comp,@typescript-eslint/no-unused-vars
  handleOpen = e => {
    this.setState({ visible: true });
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  handleCancel = e => {
    this.setState({ visible: false });
  };
}


const defaultIcon25 = [
  <Avatar size={25} key={-2} icon="user"/>,
  <Avatar size={25} style={{ backgroundColor: '#00a2ae' }}>Admin</Avatar>,
  <Avatar size={25} style={{ backgroundColor: '#FF6900' }}>USER</Avatar>,
  <Avatar size={25} style={{ color: '#f56a00', backgroundColor: '#fde3cf', }}>U</Avatar>,
  <Avatar size={25} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>,
  <Avatar size={25} src={icon1}/>,
  <Avatar size={25} src={icon2}/>,
  <Avatar size={25} src={icon3}/>,
  <Avatar size={25} src={icon4}/>,
  <Avatar size={25} src={icon5}/>,
  <Avatar size={25} src={icon6}/>,
  <Avatar size={25} src={icon7}/>,
  <Avatar size={25} src={icon8}/>,
  <Avatar size={25} src={icon9}/>,
  <Avatar size={25} src={icon10}/>,
  <Avatar size={25} src={icon11}/>,
];

const defaultIcon = [
  <Avatar key={-2} icon="user"/>,
  <Avatar style={{ backgroundColor: '#00a2ae' }}>Admin</Avatar>,
  <Avatar style={{ backgroundColor: '#FF6900' }}>USER</Avatar>,
  <Avatar style={{
    color: '#f56a00',
    backgroundColor: '#fde3cf',
  }}>U</Avatar>,
  <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>,
  <Avatar src={icon1}/>,
  <Avatar src={icon2}/>,
  <Avatar src={icon3}/>,
  <Avatar src={icon4}/>,
  <Avatar src={icon5}/>,
  <Avatar src={icon6}/>,
  <Avatar src={icon7}/>,
  <Avatar src={icon8}/>,
  <Avatar src={icon9}/>,
  <Avatar src={icon10}/>,
  <Avatar src={icon11}/>,
];

const defaultIcon50 = [
  <Avatar size={50} key={-2}><Icon type="user" style={{ fontSize: 25 }}/></Avatar>,
  <Avatar size={50} style={{ backgroundColor: '#00a2ae' }}>Admin</Avatar>,
  <Avatar size={50} style={{ backgroundColor: '#FF6900' }}>USER</Avatar>,
  <Avatar size={50} style={{
    color: '#f56a00',
    backgroundColor: '#fde3cf',
  }}>U</Avatar>,
  <Avatar size={50} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>,
  <Avatar size={50} src={icon1}/>,
  <Avatar size={50} src={icon2}/>,
  <Avatar size={50} src={icon3}/>,
  <Avatar size={50} src={icon4}/>,
  <Avatar size={50} src={icon5}/>,
  <Avatar size={50} src={icon6}/>,
  <Avatar size={50} src={icon7}/>,
  <Avatar size={50} src={icon8}/>,
  <Avatar size={50} src={icon9}/>,
  <Avatar size={50} src={icon10}/>,
  <Avatar size={50} src={icon11}/>,
];
