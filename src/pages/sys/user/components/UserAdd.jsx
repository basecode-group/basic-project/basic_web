import React, { Component } from 'react';
import { Form, Button, Drawer, Input, message, Select, Spin } from 'antd';
import AxiosUtil from '@/tool/AxiosUtil';
import SetIcon from '@/pages/sys/user/components/SetIcon';

const { Option, OptGroup } = Select;

/**
 * 添加新用户
 *
 * @author zhangby
 * @date 26/11/19 11:41 am
 */
// eslint-disable-next-line react/prefer-stateless-function
class UserAdd extends Component {
  state = {
    visible: false,
    loading: false,
    photo: '',
    userTypeList: [],
    roleList: [],
    officeList: [],
  };

  // 初始化
  addInit() {
    this.props.form.resetFields();
    this.setState({ photo: '' });
    // 查询用户类型
    AxiosUtil.get('/sys/dict/select/data/role_type')
      .then(res => {
        // eslint-disable-next-line react/no-unused-state
        this.setState({ userTypeList: res.result });
      });
    AxiosUtil.get('/sys/role/select/data')
      .then(res => {
        // eslint-disable-next-line react/no-unused-state
        this.setState({ roleList: res.result });
      });
    AxiosUtil.get('/sys/office/select/data')
      .then(res => {
        // eslint-disable-next-line react/no-unused-state
        this.setState({ officeList: res.result });
      });
    this.showDrawer();
  }

  // 提交
  // eslint-disable-next-line react/sort-comp,@typescript-eslint/no-unused-vars
  handleSubmit = e => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      // eslint-disable-next-line no-empty
      values['photo'] = this.state.photo;
      values['roleId'] = values.roleIds.join(',');
      if (!err) {
        this.setState({ loading: true });
        AxiosUtil.post('/sys/user', values)
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          .then(res => {
            message.success('新建用户成功');
            this.onClose();
            this.props.queryUserList();
            this.setState({ loading: false });
          })
          // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
          .catch(err => {
            this.setState({ loading: false });
          })
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };

    return (
      <span>
        {/* eslint-disable-next-line react/jsx-no-bind */}
        <Button icon="plus" type="primary" onClick={this.addInit.bind(this)}>新建</Button>
        {/* 弹出层 */}
        <Drawer
          title="新建用户"
          placement="right"
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 80 }}
          width={600}
        >
          <Spin spinning={this.state.loading}>
            <Form {...formItemLayout}>
             <div style={{
               textAlign: 'center',
               marginBottom: 20,
             }}>
               {/* eslint-disable-next-line react/no-unused-state,max-len */}
               <SetIcon isEdit key={this.state.photo} photo={this.state.photo} renderFun={(icon) => this.setState({ photo: icon })}/>
             </div>
             <Form.Item label="用户名">
                {getFieldDecorator('name', {
                  rules: [
                    {
                      required: true,
                      message: '请输入用户名',
                    },
                  ],
                })(<Input/>)}
              </Form.Item>
             <Form.Item label="登录名">
                {getFieldDecorator('loginName', {
                  rules: [
                    {
                      required: true,
                      message: '请输入登录名',
                    },
                    {
                      validator: this.handleDictType,
                    },
                  ],
                })(<Input/>)}
              </Form.Item>
             <Form.Item label="联系方式">
                {getFieldDecorator('mobile', {})(<Input/>)}
              </Form.Item>
             <Form.Item label="电子邮箱">
                {getFieldDecorator('email', {})(<Input/>)}
              </Form.Item>
              <Form.Item label="所属部门">
                {getFieldDecorator('officeId', {
                  rules: [
                    {
                      required: true,
                      message: '请选择部门',
                    },
                  ],
                })(
                  <Select>
                    {
                      this.state.officeList.map((item, key) => (
                        // eslint-disable-next-line react/no-array-index-key
                        <OptGroup label={item.label} key={key}>
                          {
                            item.children.map((itm, ky) => (
                              // eslint-disable-next-line react/no-array-index-key
                              <Option value={itm.value} key={ky}>{itm.label}</Option>
                            ))
                          }
                        </OptGroup>
                      ))
                    }
                  </Select>,
                )}
              </Form.Item>
              <Form.Item label="用户类型">
                {getFieldDecorator('userType', {
                  rules: [
                    {
                      required: true,
                      message: '请选择用户类型',
                    },
                  ],
                })(
                  <Select>
                    {
                      this.state.userTypeList.map((item, key) => (
                        // eslint-disable-next-line react/no-array-index-key
                        <Option key={key} value={item.value}>{item.label}</Option>
                      ))
                    }
                  </Select>,
                )}
              </Form.Item>
              <Form.Item label="所属角色">
                {getFieldDecorator('roleIds', {
                  rules: [
                    {
                      required: true,
                      message: '请选择角色',
                    },
                  ],
                })(
                  <Select mode="multiple">
                    {
                      this.state.roleList.map((item, key) => (
                        // eslint-disable-next-line react/no-array-index-key
                        <Option key={key} value={item.value}>{item.label}</Option>
                      ))
                    }
                  </Select>,
                )}
              </Form.Item>
             <Form.Item label="密码" hasFeedback>
                {getFieldDecorator('password', {
                  rules: [
                    {
                      required: true,
                      message: '请输入密码',
                    },
                    {
                      validator: this.validateToNextPassword,
                    },
                  ],
                })(<Input.Password/>)}
              </Form.Item>
              <Form.Item label="确认密码" hasFeedback>
                {getFieldDecorator('confirm', {
                  rules: [
                    {
                      required: true,
                      message: '请输入确认密码',
                    },
                    {
                      validator: this.compareToFirstPassword,
                    },
                  ],
                })(<Input.Password onBlur={this.handleConfirmBlur}/>)}
              </Form.Item>
            </Form>
          </Spin>

          {/* 提交 */}
          <div
            style={{
              position: 'absolute',
              right: 0,
              bottom: 0,
              width: '100%',
              borderTop: '1px solid #e9e9e9',
              padding: '10px 16px',
              background: '#fff',
              textAlign: 'right',
            }}
          >
            <Button onClick={this.onClose} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} type="primary">
              Submit
            </Button>
          </div>
        </Drawer>
      </span>
    )
  }

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback(' 两次输入的密码不正确!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  // eslint-disable-next-line react/sort-comp
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };
}

export default Form.create({ name: 'UserAdd' })(UserAdd);
