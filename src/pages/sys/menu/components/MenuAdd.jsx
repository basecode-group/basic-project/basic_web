import React, { Component } from 'react';
import { Form, Button, Drawer, Input, InputNumber, Switch, message, Spin } from 'antd';
import AxiosUtil from '@/tool/AxiosUtil';
import SelectIcon from '@/pages/sys/menu/components/SelectIcon';

/**
 * new menu
 *
 * @author zhangby
 * @date 27/11/19 10:38 am
 */
// eslint-disable-next-line react/prefer-stateless-function
class MenuAdd extends Component {
  state = {
    visible: false,
    loading: false,
    isShow: true,
    icon: '',
  };

  addInit() {
    this.props.form.resetFields();
    this.setState({ icon: '' });
    const { setFieldsValue } = this.props.form;
    AxiosUtil.get('/sys/menu/max/sort', { parentId: this.props.parentId }).then(res => {
      setFieldsValue({ sort: res.result.maxSort });
    });
    this.onOpen();
  }

  // 提交
  // eslint-disable-next-line react/sort-comp,@typescript-eslint/no-unused-vars
  handleSubmit = e => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({ loading: true });
        values['icon'] = this.state.icon;
        values['parentId'] = this.props.parentId;
        values.isShow = this.state.isShow ? '1' : '0';
        AxiosUtil.post('/sys/menu', values).then(() => {
          message.success('新建菜单成功');
          this.props.queryMenuList();
          this.setState({ loading: false });
          this.onClose();
        }).catch(() => {
          this.setState({ loading: false });
        })
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };

    return (
      <span>
        {/* eslint-disable-next-line react/jsx-no-bind */}
        <span onClick={this.addInit.bind(this)} style={{ cursor: 'pointer' }}>
          {
            this.props.children !== undefined ?
              this.props.children
            :
              <Button icon="plus" type="primary">新建</Button>
          }
        </span>
        {/* 弹出层 */}
        <Drawer
          title="新建菜单"
          placement="right"
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 80 }}
          width={600}
        >
          <Spin spinning={this.state.loading}>
            <Form {...formItemLayout}>
              {
                this.props.parentName !== undefined ?
                  <Form.Item label="父级菜单">
                    {getFieldDecorator('parentName', { initialValue: this.props.parentName })(<Input disabled/>)}
                  </Form.Item>
                : null
              }
              <Form.Item label="菜单名称">
                {getFieldDecorator('name', {
                  rules: [
                    {
                      required: true,
                      message: '请输入菜单名称',
                    },
                  ],
                })(<Input/>)}
              </Form.Item>
              <Form.Item label="链接">
                {getFieldDecorator('href', {
                  rules: [
                    {
                      required: true,
                      message: '请输入链接',
                    },
                  ],
                })(<Input/>)}
              </Form.Item>
              <Form.Item label="排序">
                {getFieldDecorator('sort', {
                  initialValue: 10,
                  rules: [
                    {
                      required: true,
                      message: '请输入排序',
                    },
                  ],
                })(<InputNumber min={0}/>)}
              </Form.Item>
              <Form.Item label="图标">
                {/* eslint-disable-next-line react/no-unused-state,max-len */}
                <SelectIcon key={this.state.icon} initIcon={this.state.icon} selectIcon={val => this.setState({ icon: val })} />
              </Form.Item>
              <Form.Item label="是否隐藏">
                {getFieldDecorator('isShow', { initialValue: true })(<Switch checked={this.state.isShow} onChange={ck => this.setState({ isShow: ck })} />)}
              </Form.Item>
              <Form.Item label="目标路由">
                {getFieldDecorator('component', {})(<Input/>)}
              </Form.Item>
              <Form.Item label="授权标识">
                {getFieldDecorator('permission', {})(<Input/>)}
              </Form.Item>
            </Form>
          </Spin>

          {/* 提交 */}
          <div
            style={{
              position: 'absolute',
              right: 0,
              bottom: 0,
              width: '100%',
              borderTop: '1px solid #e9e9e9',
              padding: '10px 16px',
              background: '#fff',
              textAlign: 'right',
            }}
          >
            <Button onClick={this.onClose} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} type="primary">
              Submit
            </Button>
          </div>
        </Drawer>
      </span>
    )
  }

  // eslint-disable-next-line react/sort-comp
  onOpen = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };
}

export default Form.create({ name: 'MenuAdd' })(MenuAdd);
