import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Row, Col, Button, Input, Table, Popconfirm, Icon, message, Tag, Switch } from 'antd';
import MenuAdd from '@/pages/sys/menu/components/MenuAdd';
import MenuEdit from '@/pages/sys/menu/components/MenuEdit';
import AxiosUtil from '@/tool/AxiosUtil';

const { Column } = Table;

/**
 * 菜单管理
 *
 * @author zhangby
 * @date 25/11/19 6:15 pm
 */
// eslint-disable-next-line react/prefer-stateless-function
export default class index extends Component {
  constructor(props) {
     super(props);
     this.state = {
       list: [],
       loading: false,
       query: {
         keyword: '',
       },
     };
    this.queryMenuList = this.queryMenuList.bind(this);
  }

  // 初始化
  componentWillMount() {
    this.queryMenuList();
  }

  // 查询菜单列表
  queryMenuList() {
    this.setState({ loading: true });
    AxiosUtil.get('/sys/menu', this.state.query).then(res => {
      this.setState({
        list: res.result,
        loading: false,
      });
    }).catch(() => {
      this.setState({ loading: false });
    })
  }

  // 删除
  // eslint-disable-next-line class-methods-use-this
  deleteConfirm(id) {
    AxiosUtil.delete(`/sys/menu/${id}`).then(() => {
      message.success('删除菜单成功');
      this.queryMenuList();
    });
  }

  render() {
    return (
      <div>
        <PageHeaderWrapper >
          <Card>
            {/* 功能按钮 */}
            <div style={{ marginBottom: 30 }}>
              <Row>
                <Col span={12}>
                  <MenuAdd queryMenuList={this.queryMenuList} />
                </Col>
                <Col span={12} style={{ textAlign: 'right' }}>
                  <Input.Search placeholder="菜单名称 / 链接" style={{ maxWidth: 450 }} onSearch={val => {
                    this.state.query.keyword = val;
                    this.setState({ query: this.state.query });
                    this.queryMenuList();
                  }} enterButton />
                </Col>
              </Row>
            </div>

            {/* 数据列表 */}
            <Table
              defaultExpandAllRows
              dataSource={this.state.list}
              scroll={{ x: 900 }}
              loading={this.state.loading}
              key={this.state.loading}
              rowKey="id">
              <Column width={200} align="left" title="菜单名称" dataIndex="name" render={this.menuNameRender.bind(this)}/>
              <Column width={160} align="left" title="链接" dataIndex="href" render={ val => <Tag>{val}</Tag>}/>
              <Column width={100} align="center" title="图标" dataIndex="icon" render={this.iconRender}/>
              <Column width={100} align="center" title="排序" dataIndex="sort"/>
              <Column width={100} align="center" title="隐藏" dataIndex="isShow" render={this.isShowRender}/>
              <Column width={100} align="center" title="授权" dataIndex="permission"/>
              {/* eslint-disable-next-line @typescript-eslint/no-unused-vars */}
              <Column width={160} align="center" title="操作" dataIndex="id" render={(id, record) => (
                <div>
                  {/* eslint-disable-next-line max-len */}
                  <MenuAdd queryMenuList={this.queryMenuList} parentId={id} parentName={record.name}>
                    <Button type="primary" shape="circle" icon="apartment"/>
                  </MenuAdd>
                  {/* 删除 */}
                  <Popconfirm
                    onConfirm = {this.deleteConfirm.bind(this, id)}
                    title="确定要删除记录吗？"
                    icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
                  >
                    <Button style={{ marginLeft : 10 }} type="danger" shape="circle" icon="delete"/>
                  </Popconfirm>
                </div>
              )}/>
            </Table>
          </Card>
        </PageHeaderWrapper>
      </div>
    )
  }

  // eslint-disable-next-line class-methods-use-this
  menuNameRender(val, record) {
    return (
      <MenuEdit queryMenuList={this.queryMenuList} menuId={record.id}>
        <Button type="link">{val}</Button>
      </MenuEdit>
    )
  }

  // eslint-disable-next-line class-methods-use-this
  isShowRender(val) {
    return (<Switch checked={val === '1'} size="small" />)
  }

  // eslint-disable-next-line class-methods-use-this
  iconRender(val) {
    return (<Icon type={val} style={{ fontsize: 17 }} />);
  }
}
