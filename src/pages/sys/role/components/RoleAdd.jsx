import React, { Component } from 'react';
import { Button, Modal, Form, Input, message, Select, Spin, Switch } from 'antd';
import AxiosUtil from '@/tool/AxiosUtil';

const { Option } = Select;

/**
 * 添加角色
 *
 * @author zhangby
 * @date 27/11/19 3:54 pm
 */
// eslint-disable-next-line react/prefer-stateless-function
class RoleAdd extends Component {
  state ={
    visible: false,
    spinning: false,
    useable: true,
    userTypeList: [],
  };

  addInit() {
    this.props.form.resetFields();
    // 查询用户类型
    AxiosUtil.get('/sys/dict/select/data/role_type')
      .then(res => {
        // eslint-disable-next-line react/no-unused-state
        this.setState({ userTypeList: res.result });
      });
    this.handleOpen();
  }

  // 提交
  // eslint-disable-next-line react/sort-comp,@typescript-eslint/no-unused-vars
  handleSubmit = e => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      // eslint-disable-next-line no-empty
      if (!err) {
        this.setState({ spinning: true });
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        values["useable"] = this.state.useable ? '1' : '0';
        AxiosUtil.post('/sys/role', values).then(() => {
          message.success('新建角色成功');
          this.handleCancel();
          if (this.props.queryRoleList !== undefined) {
            this.props.queryRoleList();
          }
          this.setState({ spinning: false });
        }).catch(() => {
          this.setState({ spinning: false });
        })
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };
    return (
      <span>
        {/* eslint-disable-next-line react/jsx-no-bind */}
        <Button icon="plus" type="primary" onClick={this.addInit.bind(this)}>新建</Button>
        {/* 弹出层 */}
        <Modal
          title="新建角色"
          visible={this.state.visible}
          onOk={this.handleSubmit}
          onCancel={this.handleCancel}
          confirmLoading={this.state.spinning}
          width={600}
        >
          <Spin spinning={this.state.spinning} >
           <Form {...formItemLayout}>
            <Form.Item label="角色名称">
              {getFieldDecorator('name', {
                rules: [
                  {
                    required: true,
                    message: '请输入角色名称',
                  },
                ],
              })(<Input/>)}
            </Form.Item>
            <Form.Item label="英文名称">
              {getFieldDecorator('enname', {
                rules: [
                  {
                    required: true,
                    message: '请输入英文名称',
                  },
                ],
              })(<Input/>)}
            </Form.Item>
            <Form.Item label="角色类型">
              {getFieldDecorator('roleType', {
                rules: [
                  {
                    required: true,
                    message: '请选择角色类型',
                  },
                ],
              })(
                <Select placeholder="请选择">
                  {
                    this.state.userTypeList.map((item, key) => (
                      // eslint-disable-next-line react/no-array-index-key
                      <Option key={key} value={item.value}>{item.label}</Option>
                    ))
                  }
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="是否可用">
              {/* eslint-disable-next-line max-len */}
              <Switch checked={this.state.useable} onChange={val => this.setState({ useable: val })} />
            </Form.Item>
           </Form>
          </Spin>
        </Modal>
      </span>
    );
  }

  // eslint-disable-next-line react/sort-comp,@typescript-eslint/no-unused-vars
  handleOpen = e => {
    this.setState({ visible: true });
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  handleCancel = e => {
    this.setState({ visible: false });
  };
}

export default Form.create({ name: 'RoleAdd' })(RoleAdd);
