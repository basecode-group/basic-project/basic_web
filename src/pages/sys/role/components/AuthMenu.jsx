import React, { Component } from 'react';
import { Button, Drawer, Spin, Row, Col, Tree, Icon, Tag, Switch, message } from 'antd';
import AxiosUtil from '@/tool/AxiosUtil';
import IconFont from '@/tool/IconFont';

const { TreeNode } = Tree;

/**
 * 菜单授权
 *
 * @author zhangby
 * @date 27/11/19 3:49 pm
 */
// eslint-disable-next-line react/prefer-stateless-function
export default class AuthMenu extends Component {
  state = {
    visible: false,
    loading: false,
    menuList: [],
    selectedKeys: [],
    role: {
      tails: {},
    },
  };

  authInit() {
    this.setState({ loading: true });
    // 查询角色
    AxiosUtil.get(`/sys/role/${this.props.roleId}`).then(res => {
      this.setState({ role: res.result })
    });
    AxiosUtil.get(`/sys/menu/get/for/role/${this.props.roleId}`).then(res => {
      this.setState({ selectedKeys: res.result })
    });
    // 查询菜单
    AxiosUtil.get('/sys/menu').then(res => {
      this.setState({
        menuList: res.result,
        loading: false,
      });
    }).catch(() => this.setState({ loading: false }));
    this.showDrawer();
  }

  // 提交
  // eslint-disable-next-line react/sort-comp,@typescript-eslint/no-unused-vars
  handleSubmit = e => {
    this.setState({ loading: true });
    AxiosUtil.post(`/sys/menu/save/menu/role/${this.props.roleId}`, AxiosUtil.json2FormData({ menuIds: this.state.selectedKeys.join(',') }))
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      .then(res => {
        message.success('菜单授权成功');
        this.setState({ loading: false });
      })
      // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
      .catch(err => {
        this.setState({ loading: false });
      });
  };

  // eslint-disable-next-line react/sort-comp
  render() {
    const { role } = this.state;
    return (
      <span>
        {/* eslint-disable-next-line react/jsx-no-bind */}
        <Button type="primary" shape="circle" icon="bars" onClick={this.authInit.bind(this)}/>
        {/* 弹出层 */}
        <Drawer
          title="菜单授权"
          placement="right"
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 80 }}
          width={600}
        >
          <Spin spinning={this.state.loading}>
            <h3>角色信息</h3>
            <Row style={{ marginTop: 30 }}>
              <Col span={12}>
                <IconFont type="icon-yonghu" style={{ fontSize: '1.5em', float: 'left', marginRight: 10 }}></IconFont>
                <DescriptionItem title="角色名称" content={<span style={{ color: '#eb2f96' }}>{role.name}</span>} />
              </Col>
              <Col span={12}>
                <IconFont type="icon-yingwen" style={{ fontSize: '1.4em', float: 'left', marginRight: 10, marginLeft: 2 }}></IconFont>
                <DescriptionItem title="英文名称" content={<span style={{ color: '#eb2f96' }}>{role.enname}</span>} />
              </Col>
              <Col span={12}>
                <IconFont type="icon-biaoji" style={{ fontSize: '1.5em', float: 'left', marginRight: 10 }}></IconFont>
                <DescriptionItem title="角色类型" content={<Tag color="volcano">{role.tails.roleTypeLabel}</Tag>} />
              </Col>
              <Col span={12}>
                <IconFont type="icon-jiandangzhuangtai-copy-copy" style={{ fontSize: '1.7em', float: 'left', marginRight: 8 }}></IconFont>
                <DescriptionItem title="是否可用" content={<Switch checked={role.useable === '1'} size="small" />} />
              </Col>
            </Row>
            <h3 style={{ margin: '20px 0' }}>菜单授权</h3>
            <Tree
              checkable
              defaultExpandAll
              key={this.state.loading}
              checkedKeys={this.state.selectedKeys}
              style={{ marginLeft: 30 }}
              onCheck={checkedKeys => {
                this.setState({ selectedKeys: checkedKeys });
              }}
            >
              {this.renderTreeNodes(this.state.menuList)}
            </Tree>
          </Spin>

          {/* 提交 */}
          <div
            style={{
              position: 'absolute',
              right: 0,
              bottom: 0,
              width: '100%',
              borderTop: '1px solid #e9e9e9',
              padding: '10px 16px',
              background: '#fff',
              textAlign: 'right',
            }}
          >
            <Button onClick={this.onClose} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} type="primary">
              Submit
            </Button>
          </div>
        </Drawer>
      </span>
    )
  }

  renderTreeNodes = data =>
    data.map(item => {
      if (item.children) {
        return (
          <TreeNode
            title={<div><Icon type={item.icon} style={{ marginRight: 5 }}/><span>{item.name}</span></div>}
            key={item.id} dataRef={item}>
            {this.renderTreeNodes(item.children)}
          </TreeNode>
        );
      }
      // eslint-disable-next-line max-len
      return <TreeNode title={<div><Icon type={item.icon} style={{ marginRight: 5 }}/><span>{item.name}</span></div>} key={item.id}/>;
    });


  // eslint-disable-next-line react/sort-comp
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };
}

const DescriptionItem = ({ title, content }) => (
  <div
    style={{
      fontSize: 14,
      lineHeight: '22px',
      marginBottom: 0,
      color: 'rgba(0,0,0,0.65)',
    }}
  >
    <p
      style={{
        marginRight: 8,
        marginBottom: 13,
        display: 'inline-block',
        color: 'rgba(0,0,0,0.85)',
      }}
    >
      {title}:
    </p>
    {content}
  </div>
);
