import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Alert, Button, Card, Col, Form, Input, message, Row, Switch, Table, Tag, Modal } from 'antd';
import RoleAdd from '@/pages/sys/role/components/RoleAdd';
import RoleEdit from '@/pages/sys/role/components/RoleEdit';
import AuthMenu from '@/pages/sys/role/components/AuthMenu';
import AuthUser from '@/pages/sys/role/components/AuthUser';
import AxiosUtil from '@/tool/AxiosUtil';

const { Column } = Table;

/**
 * 角色管理
 *
 * @author zhangby
 * @date 25/11/19 6:15 pm
 */
// eslint-disable-next-line react/prefer-stateless-function
class index extends Component {
  constructor(props) {
     super(props);
     this.state = {
       list: [],
       rows: [],
       pagination: {
         pageSize: 10,
         current: 1,
         total: 0,
         defaultPageSize: 10,
         showSizeChanger: true,
         onChange: (page, pageSize) => {
           this.state.query.pageNum = page;
           this.state.query.pageSize = pageSize;
           // eslint-disable-next-line react/no-access-state-in-setstate
           this.setState(this.state);
           this.queryDictList();
         },
         onShowSizeChange: (page, pageSize) => {
           this.state.query.pageNum = page;
           this.state.query.pageSize = pageSize;
           // eslint-disable-next-line react/no-access-state-in-setstate
           this.setState(this.state);
           this.queryDictList();
         },
       },
       loading: false,
       query: {
         keyword: '',
       },
     };
    this.queryRoleList = this.queryRoleList.bind(this);
  }

  componentWillMount() {
    this.queryRoleList();
  }

  // 查询角色列表
  queryRoleList() {
    this.setState({ loading: true });
    AxiosUtil.get('/sys/role', this.state.query).then(res => {
      const val = res.result;
      this.setState({
        pagination: {
          pageSize: val.size,
          current: val.current,
          total: val.total,
        },
        list: val.records,
        loading: false,
      })
    }).catch(() => this.setState({ loading: false }))
  }

  // eslint-disable-next-line class-methods-use-this
  deleteBachConfirm() {
    const { rows } = this.state;
    // eslint-disable-next-line no-underscore-dangle
    const _this = this;
    Modal.confirm({
      title: '确定要删除选择的记录吗?',
      content: `已选择${rows.length}项记录。`,
      onOk() {
        AxiosUtil.delete(`/sys/role/${rows.join(',')}`)
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          .then(res => {
            message.success('删除角色成功');
            _this.queryRoleList();
            _this.setState({ rows: [] });
          })
      },
    });
  }

  // eslint-disable-next-line react/sort-comp
  render() {
    return (
      <div>
        <PageHeaderWrapper >
          <Card>
            {/* 搜索 */}
            <Row style={{ marginBottom: 30 }}>
              <Col span={12}>
                <RoleAdd queryRoleList={this.queryRoleList} key={this.state.loading} />
                {/* 隐藏功能 */}
                {
                  this.state.rows.length > 0 ?
                    <Button type="danger" icon="delete" style={{ marginLeft: 10 }}
                            onClick={this.deleteBachConfirm.bind(this)}>批量删除</Button>
                    : null
                }
              </Col>
              <Col span={12} style={{ textAlign: 'right' }}>
                <Input.Search placeholder="菜单名称 / 链接" style={{ maxWidth: 450 }} onSearch={val => {
                  this.state.query.keyword = val;
                  // eslint-disable-next-line react/no-access-state-in-setstate
                  this.setState(this.state);
                  this.queryRoleList();
                }} enterButton />
              </Col>
            </Row>
            <Alert style={{ margin: '20px 0 20px 0' }}
                   message={
                     <div>
                       已选择 <span style={{
                       color: '#1690ff',
                       fontWeight: 600,
                       margin: '0 5px',
                     }}>{this.state.rows.length}</span> 项
                       <span style={{
                         color: '#1690ff',
                         marginLeft: 30,
                         cursor: 'pointer',
                       }} onClick={() => this.setState({ rows: [] })}>清空</span>
                     </div>
                   } type="info" showIcon/>
            {/* 列表 */}
            <Table
              defaultExpandAllRows
              dataSource={this.state.list}
              scroll={{ x: 900 }}
              loading={this.state.loading}
              key={this.state.loading}
              rowSelection={{
                selectedRowKeys: this.state.rows,
                onChange: selectedRowKeys => {
                  this.setState({ rows: selectedRowKeys })
                },
              }}
              rowKey="id">
              {/* eslint-disable-next-line react/jsx-no-bind */}
              <Column align="center" title="角色名称" dataIndex="name" render={this.roleNameRender.bind(this)}/>
              <Column align="center" title="英文名称" dataIndex="enname" />
              <Column align="center" title="角色类型" dataIndex="tails.roleTypeLabel" render={val => <Tag color="volcano">{val}</Tag>}/>
              <Column align="center" title="是否可用" dataIndex="useable" render={this.isShowRender}/>
              {/* eslint-disable-next-line @typescript-eslint/no-unused-vars */}
              <Column align="center" title="操作" dataIndex="id" render={(id, record) => (
                <div>
                  {/* eslint-disable-next-line max-len */}
                  <AuthMenu roleId={id} />
                  {/* eslint-disable-next-line max-len */}
                  <AuthUser roleId={id} />
                </div>
              )}/>
            </Table>
          </Card>
        </PageHeaderWrapper>
      </div>
    )
  }

  // eslint-disable-next-line class-methods-use-this
  roleNameRender(val, record) {
    return (
      <RoleEdit queryRoleList={this.queryRoleList} roleId={record.id}>
        <Button type="link"> {val}</Button>
      </RoleEdit>
    )
  }

  // eslint-disable-next-line class-methods-use-this
  isShowRender(val) {
    return (<Switch checked={val === '1'} size="small" />)
  }
}


export default Form.create({ name: 'index' })(index);
