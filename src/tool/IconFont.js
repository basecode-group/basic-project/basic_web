import React, { Component } from 'react';
import { Icon } from 'antd';

const IconFonts = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_1533614_wh7g3lngsyn.js',
});


/**
 *  使用第三方图库，https://www.iconfont.cn/
 */
// eslint-disable-next-line react/prefer-stateless-function
export default class IconFont extends Component {
  render() {
    return (
      <IconFonts style={{ ...this.props.style }} type={this.props.type}
                 onClick={this.props.onClick}/>
    )
  }
}
