/**
 * Ant Design Pro v4 use `@ant-design/pro-layout` to handle Layout.
 * You can view component api by:
 * https://github.com/ant-design/ant-design-pro-layout
 */
import ProLayout, { DefaultFooter } from '@ant-design/pro-layout';
import React, { useEffect } from 'react';
import Link from 'umi/link';
import { connect } from 'dva';
import { Icon } from 'antd';
import { formatMessage } from 'umi-plugin-react/locale';
import { Redirect } from 'umi';
import Authorized from '@/utils/Authorized';
import RightContent from '@/components/GlobalHeader/RightContent';
import { getAuthorityFromRouter, isAntDesignPro } from '@/utils/utils';
import logo from '../assets/logo.svg';
import $ from 'jquery';
import defaultSettings from '../../config/defaultSettings';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const { http } = defaultSettings;

// const noMatch = (
//   <Result
//     status="403"
//     title="403"
//     subTitle="Sorry, you are not authorized to access this page."
//     extra={
//       <Button type="primary">
//         <Link to="/user/login">Go Login</Link>
//       </Button>
//     }
//   />
// );

const noMatch = (
  <Redirect to="/login"/>
);

/**
 * use Authorized check all menu item
 */
// const menuDataRender = menuList =>
//   menuList.map(item => {
//     const localItem = {
//       ...item,
//       children: item.children ? menuDataRender(item.children) : [],
//     };
//     return Authorized.check(item.authority, localItem, null);
//   });

// eslint-disable-next-line @typescript-eslint/no-unused-vars
// const menuDataRender = menuList => [
//    {
//      path: '/sys',
//      name: '系统管理',
//      icon: 'setting',
//      children: [
//        {
//          path: '/sys/user',
//          name: '用户管理',
//          icon: 'user',
//        },
//      ],
//    },
//  ];
const menuDataRender = () => {
  // eslint-disable-next-line no-redeclare
  let menuData = [];
  $.ajax({
    url: `${http}/sys/menu/get/for/user`,
    data: {},
    dataType: 'json',
    cache: false,
    async: false,
    headers: { Authorization: localStorage.getItem('Authorization') },
    type: 'GET',
    success (res) {
      menuData = res.result;
    },
    error (err) {
      console.log(`菜单获取异常：${JSON.stringify(err)}`)
    },
  });
  return menuData;
};

const defaultFooterDom = (
  <DefaultFooter
    copyright="2019 蚂蚁金服体验技术部出品"
    links={[
      {
        key: 'Ant Design Pro',
        title: 'Ant Design Pro',
        href: 'https://pro.ant.design',
        blankTarget: true,
      },
      {
        key: 'github',
        title: <Icon type="github"/>,
        href: 'https://github.com/ant-design/ant-design-pro',
        blankTarget: true,
      },
      {
        key: 'Ant Design',
        title: 'Ant Design',
        href: 'https://ant.design',
        blankTarget: true,
      },
    ]}
  />
);

const footerRender = () => {
  if (!isAntDesignPro()) {
    return defaultFooterDom;
  }

  return (
    <>
      {defaultFooterDom}
      <div
        style={{
          padding: '0px 24px 24px',
          textAlign: 'center',
        }}
      >
        <a href="https://www.netlify.com" target="_blank" rel="noopener noreferrer">
          <img
            src="https://www.netlify.com/img/global/badges/netlify-color-bg.svg"
            width="82px"
            alt="netlify logo"
          />
        </a>
      </div>
    </>
  );
};

const verifyLogin = () => {
  $.ajax({
    url: `${http}/sys/oauth/login/verify`,
    data: {},
    dataType: 'json',
    cache: false,
    async: false,
    headers: { Authorization: localStorage.getItem('Authorization') },
    type: 'GET',
    success(res) {
      if (res.result < 1) {
        window.location.href = '/login';
      }
    },
    error(err) {
      window.location.href = '/login';
      console.log(`登录验证异常：${JSON.stringify(err)}`)
    },
  });
};

const BasicLayout = props => {
  const {
    dispatch,
    children,
    settings,
    location = {
      pathname: '/',
    },
  } = props;
  /**
   * constructor
   */

  useEffect(() => {
    if (dispatch) {
      dispatch({
        type: 'user/fetchCurrent',
      });
      dispatch({
        type: 'settings/getSetting',
      });
    }
  }, []);
  /**
   * init variables
   */

  const handleMenuCollapse = payload => {
    if (dispatch) {
      dispatch({
        type: 'global/changeLayoutCollapsed',
        payload,
      });
    }
  }; // get children authority

  const authorized = getAuthorityFromRouter(props.route.routes, location.pathname || '/') || {
    authority: undefined,
  };

  // 验证登录
  verifyLogin();

  return (
    <ProLayout
      logo={logo}
      onCollapse={handleMenuCollapse}
      menuItemRender={(menuItemProps, defaultDom) => {
        if (menuItemProps.isUrl || menuItemProps.children) {
          return defaultDom;
        }

        return <Link to={menuItemProps.path}>{defaultDom}</Link>;
      }}
      breadcrumbRender={(routers = []) => [
        {
          path: '/',
          breadcrumbName: formatMessage({
            id: 'menu.home',
            defaultMessage: 'Home',
          }),
        },
        ...routers,
      ]}
      itemRender={(route, params, routes, paths) => {
        const first = routes.indexOf(route) === 0;
        return first ? (
          <Link to={paths.join('/')}>{route.breadcrumbName}</Link>
        ) : (
          <span>{route.breadcrumbName}</span>
        );
      }}
      footerRender={footerRender}
      menuDataRender={menuDataRender}
      formatMessage={formatMessage}
      rightContentRender={rightProps => <RightContent {...rightProps} />}
      {...props}
      {...settings}
    >
      <Authorized authority={authorized.authority} noMatch={noMatch}>
        {children}
      </Authorized>
    </ProLayout>
  );
};

export default connect(({ global, settings }) => ({
  collapsed: global.collapsed,
  settings,
}))(BasicLayout);
