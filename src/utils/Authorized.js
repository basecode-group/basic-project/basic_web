import RenderAuthorize from '@/components/Authorized';
import $ from 'jquery';
import defaultSettings from '../../config/defaultSettings';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const { http } = defaultSettings;

let authority = [];

$.ajax({
  url: `${http}/sys/oauth/login/verify`,
  data: {},
  dataType: 'json',
  cache: false,
  async: false,
  headers: { Authorization: localStorage.getItem('Authorization') },
  type: 'GET',
  success (res) {
    authority = res.result;
    if (authority.length < 1) {
      // window.location.href = '/login';
    }
  },
  error (err) {
    console.log(`登录验证异常：${JSON.stringify(err)}`)
  },
});

// eslint-disable-next-line import/no-mutable-exports
let Authorized = RenderAuthorize(authority); // Reload the rights component

const reloadAuthorized = () => {
  Authorized = RenderAuthorize(authority);
};

export { reloadAuthorized };
export default Authorized;
